# Alpine-base

## Description

Docker alpine base image with custom user, user management and entrypoint scripts.

[Source](https://gitlab.com/dotslashme/docker-alpine-base/) | [Issues](https://gitlab.com/dotslashme/docker-alpine-base/-/issues) | [Vulnerabilities](https://gitlab.com/dotslashme/docker-alpine-base/-/security/vulnerability_report)

### Versioning

The version tag always follow alpine versioning, meaning the tag `:3` of this image builds upon `alpine:3`.

## Binaries

The following binaries are installed in addition to what comes with the `alpine` image:

- **linux-pam** - needed to handle user account
- **shadow** - needed to be able to create/change users
- **su-exec** - needed to be able to execute binaries as another user


## Features

- Custom user (dockrun) with customizable `UID:GID`.
- Provides a custom entrypoint at `/docker-entrypoint.sh` that will run `CMD`s as the dockrun user.

### Custom user

The `uid` and `gid` can be set using the `PUID` and `PGID` environmental variables in your docker-compose file or directly on the command line.

The `uid` and `gid` are set to `1000` by default, unless overridden.

The UID:GID change is accomplished through the [docker-entrypoint.sh](https://gitlab.com/dotslashme/docker-alpine-base/-/blob/main/docker-entrypoint.sh) script.

### Custom entrypoint

A custom entrypoint has been installed in `/docker-entrypoint.sh`, which will run any `CMD` you define in your own
Dockerfile, with the user `dockrun`.
