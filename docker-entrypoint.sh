#!/usr/bin/env sh

# Set uid/gid to supplied values or use default
UID=${PUID:-1000}
GID=${PGID:-1000}

groupmod -o -g "$GID" dockrun
usermod -o -u "$UID" dockrun

printf "User dockrun is running with the following IDs:\n"
printf "\tUID: %s\n" "${UID}"
printf "\tGID: %s\n" "${GID}"

# execute CMD as dockrun user
exec su-exec dockrun "$@"
